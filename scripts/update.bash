#!/bin/bash

# Stupid hack because updating overwrites this script.
if [ "$0" != "/tmp/update" ]; then
    cp /opt/nagyvpn/scripts/update /tmp/update
    exec /tmp/update
fi

git clone https://gitlab.com/lnagy97/nagyvpn /opt/nagyvpn

service openvpn reload
exec rm -f /tmp/update
