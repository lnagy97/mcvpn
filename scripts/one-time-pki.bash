#!/bin/bash

set -eu

if [ ! -f /etc/openvpn/state/ta.key ]; then
    # EasyRSA variables
    cat > /etc/openvpn/state/vars <<EOF
set_var EASYRSA_REQ_COUNTRY     "US"
set_var EASYRSA_REQ_PROVINCE    "California"
set_var EASYRSA_REQ_CITY        "San Francisco"
set_var EASYRSA_REQ_ORG         "Nagy VPN Co."
set_var EASYRSA_REQ_EMAIL       "lorand.nagy@nagyinv.com"
set_var EASYRSA_REQ_OU          "Head of Everything"
EOF

    cd /etc/openvpn/state
    /opt/easyrsa/easyrsa --batch init-pki
    /opt/easyrsa/easyrsa --batch --req-cn='vpn-ca@vpn.nagyinv.com' build-ca nopass
    /opt/easyrsa/easyrsa gen-dh
    # Server certificate
    /opt/easyrsa/easyrsa --batch --req-cn='vpn.nagyinv.com' gen-req \
        'vpn@nagyinv.com' nopass
    /opt/easyrsa/easyrsa --batch sign-req server vpn@nagyinv.com
    # Generate empty CRL
    /opt/easyrsa/easyrsa gen-crl
    # Shared secret to prevent ddos
    openvpn --genkey --secret ta.key
fi
