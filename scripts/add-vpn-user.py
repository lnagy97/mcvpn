#!/usr/bin/env python

import smtplib
from os.path import basename
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate

import subprocess
import os
import sys
import string
import shutil
import tempfile
import random

def _run(working_dir, command):
    process = subprocess.Popen(command, cwd=working_dir)
    if process.wait():
        print 'Command: {} failed with exit status: {}'.format(cmd.join(' '), process.returncode)
        sys.exit(-1)

def _easyrsa(args):
    _run('/etc/openvpn/state', ['/opt/easyrsa/easyrsa', '--batch'] + args)

def _mkpass():
    return ''.join(random.choice(string.ascii_lowercase + string.ascii_uppercase + string.digits) for _ in range(32))

# Curtesy of stackoverflow
# https://stackoverflow.com/questions/3362600/how-to-send-email-attachments
def _sendmail(user, archive):
    msg = MIMEMultipart()
    msg['From'] = 'lorand.nagy@nagyinv.com'
    msg['To'] = user
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = 'VPN: Your personal certificate'

    user_short = user.split('@')[0]
    pretty_name = user_short.split('.')[0].capitalize()

    msg.attach(MIMEText("""Hi {},

You can find your personal NagyInv credentials attached to this email.
Use the password to decrypt.

Best Regards,
Floobidyhero""".format(pretty_name, user_short)))

    with open(archive, 'rb') as fil:
        part = MIMEApplication(
            fil.read(),
            Name=basename(archive)
        )
    part['Content-Disposition'] = 'attachment; filename="%s"' % basename(archive)
    msg.attach(part)

    smtp = smtplib.SMTP('smtp-relay.gmail.com', 587)
    smtp.ehlo()
    smtp.starttls()
    smtp.ehlo()
    smtp.sendmail('lorand.nagy@nagyinv.com', user, msg.as_string())
    smtp.close()


if len(sys.argv) != 2:
    print 'Usage: {} <email_with_domain>'.format(sys.argv[0])
    sys.exit(-1)

user = sys.argv[1]
passwd = _mkpass()
archive = '/tmp/{}.zip'.format(user)

_easyrsa(['--req-cn={}'.format(user), 'gen-req', user, 'nopass'])
_easyrsa(['sign-req', 'client', user])

tmpdir = tempfile.mkdtemp()
shutil.copyfile('/etc/openvpn/state/pki/ca.crt', os.path.join(tmpdir, 'ca.crt'))
shutil.copyfile('/etc/openvpn/state/ta.key', os.path.join(tmpdir, 'ta.key'))
shutil.copyfile('/etc/openvpn/state/pki/private/{}.key'.format(user), os.path.join(tmpdir, 'client.key'))
shutil.copyfile('/etc/openvpn/state/pki/issued/{}.crt'.format(user), os.path.join(tmpdir, 'client.crt'))
shutil.copyfile('/opt/nagyvpn/conf/client.conf', os.path.join(tmpdir, 'client.ovpn'))

if os.path.isfile(archive):
    os.remove(archive)

_run(tmpdir, ['zip', '-P', passwd, archive, '-r', '.'])
shutil.rmtree(tmpdir)

print 'Vpn config saved to: {}'.format(archive)
print 'Password is: {}'.format(passwd)

_sendmail(user, archive)
