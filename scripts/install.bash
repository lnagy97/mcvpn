#!/bin/bash

# Associate public IP address
local_ipv4="$(curl -s http://3.18.193.85/latest/meta-data/local-ipv4)"
instance_id="$(curl -s http://3.18.193.85/latest/meta-data/instance-id)"
elastic_ip_id="eipassoc-0d4f4949a3143c3f5"
aws ec2 associate-address --allocation-id "$elastic_ip_id" --instance-id "$instance_id" --allow-reassociation --private-ip-address "$local_ipv4" --region us-east-2


# Install openvpn
yum install -y openvpn python27-boto3
perl -pi -e 's/net.ipv4.ip_forward = 0/net.ipv4.ip_forward = 1/' /etc/sysctl.conf
sysctl -p
# Download + extract to /opt
curl -sL https://github.com/OpenVPN/easy-rsa/releases/download/v3.0.4/EasyRSA-3.0.4.tgz | tar -zxf - -C /opt
ln -s /opt/EasyRSA-3.0.4 /opt/easyrsa

# EasyRSA variables
cat > /etc/openvpn/vars <<EOF
set_var EASYRSA_REQ_COUNTRY     "US"
set_var EASYRSA_REQ_PROVINCE    "California"
set_var EASYRSA_REQ_CITY        "San Francisco"
set_var EASYRSA_REQ_ORG         "Nagy VPN Co."
set_var EASYRSA_REQ_EMAIL       "lorand.nagy@nagyinv.com"
set_var EASYRSA_REQ_OU          "Head of Everything"
EOF

# Create mount points, and update fstab
mkdir /etc/openvpn/state
mkdir /var/log/openvpn

cat >> /etc/fstab <<EOF
fs-94fa93ed.efs.us-east-2.amazonaws.com:/openvpn/state /etc/openvpn/state nfs4 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 0 0
fs-94fa93ed.efs.us-east-2.amazonaws.com:/openvpn/log /var/log/openvpn nfs4 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 0 0
EOF
mount -a

# Initialize pki if needed.
/opt/nagyvpn/scripts/one-time-pki

# Link admin scripts to /usr/local/sbin
ln -s /opt/nagyvpn/scripts/add-vpn-user /usr/local/sbin/add-vpn-user
ln -s /opt/nagyvpn/scripts/remove-vpn-user /usr/local/sbin/remove-vpn-user

# Server config
ln -s /opt/nagyvpn/conf/server.conf /etc/openvpn/server.conf

# NAT outbound connections on eth0
iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE
# Persist iptables ruleset
service iptables save
# Make openvpn default on
chkconfig openvpn on
# Start openvpn
service openvpn start
